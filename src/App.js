import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import Layout from './components/Layout';
import EventCard from './components/EventCard';

const GET_EVENTS = gql`
  {
    getEvents{
      _id,
      title,
      createdAt,
      created_by{
        first_name,
        last_name,
      }
    }
  }
`;

function App() {
  const { loading, data, error } = useQuery(GET_EVENTS);

  if (loading) return <h1>Cargando</h1>
  if (error) return <h3>{error.message}</h3>

  return (
      <Layout
        title="Clone de Meetup"
        subtitle="¿Que vamos a hacer hoy?"
      >
        {
          data.getEvents.map((event) => (
            <EventCard
              title={event.title}
              author={`${event.first_name} ${event.last_name}`}
              date={event.createAt}
            />
          ))
        }
      </Layout>
  );
}

export default App;
