import React, { Fragment } from 'react';
import Navbar from '../Navbar';
import MasterHead from '../MasterHead';

export function Layout({ title, subtitle, children }){
  return(
    <Fragment>
      <Navbar />
      <MasterHead 
        title={title}
        subtitle={subtitle}
      />
      <div className="container">
        <div className="row">
          <div className="col-lg-8 col-md-10 mx-auto">
            {children}
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default React.memo(Layout)