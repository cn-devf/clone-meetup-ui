import React from 'react';
import PropTypes from 'prop-types';
import Background from '../../assets/img/home-bg-2.jpg';

export function MasterHead({ title, subtitle }) {
  return(
    <header className="masthead" style={{ backgroundImage: `url(${Background})`}}>
      <div className="overlay"></div>
      <div className="container">
        <div className="row">
          <div className="col-lg-8 col-md-10 mx-auto">
            <div className="site-heading">
              <h1>{title}</h1>
              <span className="subheading">
                {subtitle}
              </span>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
}

MasterHead.propTypes = {
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string,
};

MasterHead.defaultProps = {
  title: 'Titulo',
}

export default MasterHead;
