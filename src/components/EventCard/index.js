import React, { Fragment } from 'react';

export function EventCard({ title, subtitle, author, date }){
  return (
    <Fragment>
      <div className="post-preview">
        <a href="post.html">
          <h2 className="post-title">
            {title}
          </h2>
          <h3 className="post-subtitle">
            {subtitle}
          </h3>
        </a>
        <p className="post-meta">Posted by <a>{author}</a> on {date}</p>
      </div>
      <hr />
    </Fragment>
  );
}

export default React.memo(EventCard);
