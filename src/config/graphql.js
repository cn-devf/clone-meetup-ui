import ApolloClient from 'apollo-boost';

export default new ApolloClient({
  uri: 'https://cn-devf-clone-netflix.herokuapp.com/',
});